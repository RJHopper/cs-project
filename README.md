# HOW TO USE
1. clone/download the project
2. cd cs-project and run \_\_main\_\_.py
3. install dependencies if necesarry


## dependencies
- curses (windows-curses if you are on windows)
- pandas
- mysql connector
- make sure you have a working mySQL installation that you can access


# USAGE

## 1. Convert excel sheet to SQL
Run converter.py
1. Select excel file and which sheets to convert
2. Log into SQL with the prompt
3. Let it convert

## 2. Edit SQL table
***still under construction, may never be finished***


## WHY DOES THIS EXIST?
- CBSE project for 12th needs something
- excel can be slow for data analysis
- SQL should be faster so this should help

## WHO IS DOING THIS
- Rohan J Hopper
- Ruhaan Matthews
- Sohan TP

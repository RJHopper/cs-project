import mysql.connector as mc
import random #for random passwd gen

def init(username,password):
    connection=mc.connect(host="localhost",user=username,password=password)
    curs=connection.cursor()

    init.curs=curs
    init.connection=connection

def end():
    curs=init.curs
    connection=init.connection

    curs.close()
    connection.commit()
    connection.close()


def createUser():
    '''
    Creates user with access to one database/new database.
    Generates a random password and saves it in config.py .
    '''
    curs=init.curs
    alphaNum="abcdefghijklmnopqrstuvwxyz1234567890"
    newPass=""
    for i in range(16):
        newPass+=random.choice(alphaNum)
    querry="create user \'ExcelToSQLProgram\'@\'localhost\' IDENTIFIED BY \'" + newPass + "\';"

    dbname="convertedTables"
    createDbCommand="create database " + dbname + ";"
    dbPermissionString="grant all privileges on " + dbname + ".* to 'ExcelToSQLProgram'@'localhost'"

    with open('config.py','w') as f:
        f.write("sqlUser=\"ExcelToSQLProgram\"\n")
        f.write("sqlPass=\""+newPass+"\"\n")
        f.write("DBName=\"" +dbname +"\"\n")
    
    curs.execute(querry)
    curs.execute(createDbCommand)
    curs.execute(dbPermissionString)

def useDB(database):
    curs=init.curs
    querry="use " + database + ";"
    curs.execute(querry)


def createTable(tablename,types,names):
    curs=init.curs
    newnames = []

    for b in range(len(names)):
        new = names[b].replace(" ", "_")
        newnames.append(new)
        
    query="CREATE TABLE " + tablename + " ("
    for i in range(len(types)-1):
        query += newnames[i]+ " " + types[i]+","
    query += newnames[len(types)-1]+ " " + types[len(types)-1]
    query += ");"
    curs.execute(query)


def insertinto_sqltab(tablename,values):
    curs=init.curs
    values_inserted = ""
    for i in range(len(values)):
        value = str(values[i])
        if value == 'Null':
            values_inserted += value
        else:
            values_inserted += '"'
            values_inserted += value
            values_inserted += '"'

        if i < len(values)-1:
            values_inserted += ","
        elif i == len(values):
            break

    query = "INSERT INTO " + tablename + " VALUES (" + values_inserted + ");"
    curs.execute(query)

import curses



def init():
    stdscr = curses.initscr()
    curses.cbreak()
    stdscr.keypad(True)
    curses.start_color()
    curses.noecho()
    
    columns,lines = stdscr.getmaxyx()


    init.stdscr = stdscr
    init.columns = columns
    init.lines = lines
    win = curses.newwin(0,0)
    win.border()
    init.win = win

# init(stdscr,columns,lines,0,0)


def generateList(options):
    '''
    Paints a sequence into the viewport as a vertical list
    '''
    win = init.win
    win.border()
    y=1
    for i in options:
        win.addstr(y,1,str(i))
        y+=1
    win.refresh()


def scroll(options):
    '''
    Arrow key input is used to scroll through list passed to it.
    The list should already be there from generate-list
    Returns selected option if you press enter
    If you press q(for quit) returns None
    '''
    win = init.win
    cursorPos = 0
    win.refresh()
    # win.addstr(cursorPos+1,1,options[cursorPos],curses.A_UNDERLINE) #testing line
    while True:
        inp = win.getch()
        # win.addstr(5,20,str(inp)) #this line used to test keys
        if inp in [ord('k'),curses.KEY_UP,65] and cursorPos>0:
            #scroll UP
            win.addstr(cursorPos+1,1,str(options[cursorPos]))
            cursorPos-=1
            win.addstr(cursorPos+1,1,str(options[cursorPos]), curses.A_UNDERLINE)
        elif inp in [ord('j'),curses.KEY_DOWN,66] and cursorPos<(len(options)-1):
            #scroll DOWN
            win.addstr(cursorPos+1,1,str(options[cursorPos]))
            cursorPos+=1
            win.addstr(cursorPos+1,1,str(options[cursorPos]), curses.A_UNDERLINE)
        elif inp in [curses.KEY_ENTER,10,'\n']:
            return options[cursorPos]
            break
        elif inp in [ord('q')]:
            return None
            break
        win.refresh()


def selectOption(inputList):
    '''
    combines generateList and scroll
    '''
    generateList(inputList)
    output = scroll(inputList)
    return output


def endUI():
    '''
    ends the UI? idt this needs a docstring/comment
    ''' 
    curses.nocbreak()
    init.stdscr.keypad(False)
    curses.echo()
    
    curses.endwin()



def textWindow(text,y,x):
    '''
    this should draw a window at (y,x) and put text in it
    I think it was just a test function but it might come in handy later
    ''' 
    win = curses.newwin(len(text),len(text),y,x)

    win.border()
    win.addstr(0,0,text)
    win.refresh()
    del win


def getUsernamePass():
    '''
    Generates a prompt for username and password.
    Username is shown
    Characters are printed as starts for password entry.
    '''
    win = init.win
    curses.echo()
    win.addstr(1,1,"Enter SQL username:")
    win.addstr(2,1,"Enter SQL password:")
    win.addstr(3,1,"Don't worry, this information will not be stored")

    username = win.getstr(1,20)
    username = username.decode()


    # Password entry:
    curses.noecho()
    password = ''
    win.move(2,20)
    cursorPos=20
    passArray=[]
    
    while True:
        inp = win.getch()
        if inp in [curses.KEY_ENTER,10,'\n']:
            for i in passArray:
                password+=str(i)        
            break
        elif inp in [curses.KEY_BACKSPACE,127,'^?'] and passArray:
            passArray.pop()
            cursorPos-=1
            win.addstr(2,cursorPos," ")
        elif inp in [curses.KEY_BACKSPACE,127,'^?'] and not passArray:
            pass
        else:
            passArray.append(chr(inp))
            win.addstr(2,cursorPos,"*")
            cursorPos+=1
            curses.beep()
        # win.addstr(5,20,str(len(passArray))) #this line used to test keys

    curses.noecho()
    return username,password










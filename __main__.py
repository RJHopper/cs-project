import os
import UI
import SQLcommands

dependencies={"curses":"windows-curses","pandas":"pandas","mysql.connector":"mysql-connector-python"}

toInstall=[]

def tryModule(module):
    try:
        __import__(module)
    except:
        toInstall.append(dependencies[module])



   
if os.path.exists("config.py"):     #The config file would only be created if all dependencies are met. SO its presence is used to decide whether to check or not.
    pass

else:
    print("Checking for modules...")
    for i in dependencies:
        tryModule(i)

    if toInstall:
        print("Dependecies NOT met\nPlease install with the command: pip install ",end="")
        for i in toInstall:
            print(i, end=" ")
            
        print()
        exit()
    else:
        print("All dependencies satisfied")

    UI.init()
    username,password = UI.getUsernamePass()
    UI.endUI()
    
    SQLcommands.init(username,password)   
    del username,password

    SQLcommands.createUser()
    SQLcommands.end()
    
    print("The program succesfully has made itself its own mySQL user and database.")


if input("Do you want to proceed with creating a table? [y/N]: ")=="y":
    import converter
    converter.main()

from os import scandir #to find file
import UI #custom module
import pandas as pd #for excel sheet reading
import SQLcommands

def getSheetNames(filename):
    rxl = pd.ExcelFile(filename)
    xlsn = rxl.sheet_names
    return(xlsn)


def loadfile(filename):
    rxl = pd.read_excel(filename)
    return(rxl)


def extractSheet(filename, sheetName):
    rxl_sheet = pd.read_excel(filename, sheet_name=sheetName)
    return(rxl_sheet)


def extract_heading(filename, sheetName):
    rxl = pd.read_excel(filename, sheet_name=sheetName)
    row = rxl.head()
    heading = []
    for i in row:
        heading.append(i)
    return heading


def colextract(filename,sheet):
    col=[]
    df_col = pd.read_excel(filename, sheet_name=sheet).columns
    for i in df_col:
        col.append(i)
    return(col)


def getdt(filename,sheet):
    '''
    Extracts the datatypes of each excel column and translates it to mySQL datatypes.
    '''
    rxl = pd.read_excel(filename, sheet_name=sheet)
    dt = rxl.dtypes
    dnew = []
    
    for i in dt:
        i=str(i)
        # print(i,end=' ') #test
        if i == 'int64':
            dnew.append('INT')
        elif i == 'O' or i == 'object':
            dnew.append('VARCHAR(255)')
        elif i == 'datetime64[ns]':
            dnew.append('DATETIME')
        elif i == 'float64':
            dnew.append('DECIMAL(10,10)')
        else:
            print("Unsupported datatype")
            dnew.append('')
    return dnew


def extract_row(filename, row_no):
    rxl = pd.read_excel(filename)
    df = pd.DataFrame(rxl)
    row = df.loc[row_no-1]
    frow = []
    for i in row:
        i=str(i)
        if i == "nan" or i=='NaT':
            frow.append("Null")
        else:
            frow.append(i)

    # for c in range(len(newnames)):
        # if "" or '' in newnames:
            # newnames.remove('') or newnames.remove("")
        # 
    return(frow)


def fileSelector():
    '''
    Returns list of files in current directory
    Used to select the excel file.
    '''
    ls=[]
    for i in scandir():
        i = str(i)
        ls.append(i[11:-2])
    return(ls)


def main():
    UI.init()
    xlFile=UI.selectOption(fileSelector())
    UI.endUI()

    if xlFile==None or ".xls" not in xlFile:
        print("Not an excel file. Exiting...")
        exit()

    # xls = loadfile(filename = xlFile)
    sheets = getSheetNames(xlFile)

    if len(sheets)==1:
        sheet=sheets[0]
    else:
        UI.init()
        sheet=UI.selectOption(sheets)
        UI.endUI()
    
    column_names = extract_heading(xlFile, sheet)
    column_dtypes = getdt(xlFile, sheet)

    

    import config
    SQLcommands.init(config.sqlUser,config.sqlPass)
    SQLcommands.useDB(config.DBName)

    table=xlFile.replace(" ", "_").replace(".xlsx", " ").replace(".xls"," ").replace("-", "_")

    SQLcommands.createTable(table,column_dtypes,column_names)
    
    
    row=extract_row(xlFile,1)
    i=1
   
    while row:
        try:
            row=extract_row(xlFile,i)
            # print(row) #test
        except:
            # print("empty row") #test
            break
        else:
            # print("row added, moving on") #test
            i+=1    
        SQLcommands.insertinto_sqltab(table,row)
       
    SQLcommands.end()

    

if __name__ == "__main__":
    main()

